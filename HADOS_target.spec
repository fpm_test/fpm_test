Name:		HADOS
Version:	1.0
Release:	1
Summary:	HADOS 

Group:		HADOS
License:	GPL

Source0: HADOS_target.tar.gz

%define __check_files %{nil}
%define HADOS_VER HADOS

%if "%{_repository}" == "Ubuntu_20.04"
BuildRequires:python3,python3-requests,python3-yaml
%else
BuildRequires:python3,python3-requests,python3-pyyaml
%endif

%if 0%{?rhel} >= 8 || "%{_vendor}" == "openEuler"
BuildRequires: createrepo
%else
BuildRequires: 
%endif

%description
HADOS integration tool

%prep
%setup -q -n HADOS_target

%build
cd %{_builddir}/HADOS_target
%{__mkdir_p} %{HADOS_VER}

python3 download.py -q %{HADOS_VER} %_repository

%install
%{__install} -m 0755 -d %{buildroot}/opt/yusur
%{__install} -m 0755 -d %{buildroot}/opt/yusur/repo
%{__install} -m 0755 -d %{buildroot}/opt/yusur/hados
%{__install} -m 0755 -d %{buildroot}/opt/yusur/helm

%if 0%{?rhel} >= 8 || "%{_vendor}" == "openEuler"
%{__install} -m 0755 -d %{buildroot}%{_sysconfdir}/yum.repos.d
%{__install} -p -m 0644 -t %{buildroot}%{_sysconfdir}/yum.repos.d/ hados.repo
%else
%{__install} -m 0755 -d %{buildroot}%{_sysconfdir}/apt/sources.list.d
%{__install} -p -m 0644 -t %{buildroot}%{_sysconfdir}/apt/sources.list.d/ hados.list
%endif

%if "%{_repository}" == "Ubuntu_20.04"

%{__install} %{HADOS_VER}/%{_repository}/*.deb %{buildroot}/opt/yusur/repo

%else

%{__install} %{HADOS_VER}/%{_repository}/*.rpm %{buildroot}/opt/yusur/repo

%endif

python3 settle_file.py -s %_repository %{HADOS_VER}/containers/ %{buildroot}/opt/yusur/hados/

%if "%{_repository}" == "openEuler-22.03-LTS" || "%{_repository}" == "CentOS-Stream-8" || "%{_repository}" == "Ubuntu_20.04"

################## hados_swift2100r-host #############################
%package -n hados_swift2100r-host
Summary: HADOS hados_swift2100r-host
%description -n hados_swift2100r-host
%files -n hados_swift2100r-host
/opt/yusur/repo/*rdma*
/opt/yusur/repo/*nic*
/opt/yusur/hados/*
%if 0%{?rhel} >= 8 || "%{_vendor}" == "openEuler"
%{_sysconfdir}/yum.repos.d/hados.repo
%else
%{_sysconfdir}/apt/sources.list.d/hados.list
%endif
################## hados_swift2100r-host #############################

################## hados_flexflow2100p-host ##########################
%package -n hados_flexflow2100p-host
Summary: HADOS hados_flexflow2100p-host
%description -n hados_flexflow2100p-host
%files -n hados_flexflow2100p-host
/opt/yusur/repo/*dpdk*
/opt/yusur/repo/*ovs*
/opt/yusur/repo/*doe*
/opt/yusur/repo/*vfus*
/opt/yusur/repo/*nic*
/opt/yusur/hados/*
%if 0%{?rhel} >= 8 || "%{_vendor}" == "openEuler"
%{_sysconfdir}/yum.repos.d/hados.repo
%else
%{_sysconfdir}/apt/sources.list.d/hados.list
%endif
################## hados_flexflow2100p-host ##########################

%endif

%if "%{_repository}" == "openEuler-22.03-LTS_dpu"

################## hados_flexflow2100p-dpu ###########################
%package -n hados_flexflow2100p-dpu
Summary: HADOS hados_flexflow2100p-dpu
%description -n hados_flexflow2100p-dpu
%files -n hados_flexflow2100p-dpu
/opt/yusur/repo/*dpdk*
/opt/yusur/repo/*spdk*
/opt/yusur/repo/*ovs*
/opt/yusur/repo/*doe*
/opt/yusur/repo/*bondctl*
/opt/yusur/repo/*nic*
/opt/yusur/hados/*
%{_sysconfdir}/yum.repos.d/hados.repo
################## hados_flexflow2100p-dpu ###########################

%endif

%if "%{_repository}" == "Ubuntu_20.04"

%post -n hados_swift2100r-host
cd /opt/yusur/ && dpkg-scanpackages repo /dev/null | gzip > /opt/yusur/repo/Packages.gz
apt-get update
%post -n hados_flexflow2100p-host
cd /opt/yusur/ && dpkg-scanpackages repo /dev/null | gzip > /opt/yusur/repo/Packages.gz
apt-get update

%endif
%if "%{_repository}" == "openEuler-22.03-LTS" || "%{_repository}" == "CentOS-Stream-8"

%post -n hados_swift2100r-host
createrepo /opt/yusur/repo/
%post -n hados_flexflow2100p-host
createrepo /opt/yusur/repo/

%endif
%if "%{_repository}" == "openEuler-22.03-LTS_dpu"

%post -n hados_flexflow2100p-dpu
createrepo /opt/yusur/repo/

%endif

%changelog
* Tue Aug 8 2023 LiangShiWei <liangsw@yusur.tech> 1.4
- Add a parsing script for the yaml file

* Mon Aug 7 2023 zhangshuai <zhangs@yusur.tech> 1.3
- Optimized code

* Mon Jul 31 2023 liuhaichang <liuhc@yusur.tech> 1.2
- Optimized code

* Thu Jul 27 2023 ChenYongBin <chenyb@yusur.tech> 1.1
- Add multi product packaging

* Tue Feb 28 2023 SunXuDong <sunxd@yusur.tech> 1.0
- Initial HADOS tool
