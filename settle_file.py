#!/usr/bin/env python
# coding=utf-8

'''
Author: liangsw@yusur.tech
Date: 2023-10-07 16:18:56
LastEditTime: 2023-11-23 09:31:57
LastEditors: liangsw@yusur.tech
Description: 
Copyright (c) 2023 by liangsw email: liangsw@yusur.tech, All Rights Reserved.
'''

import os
import os.path
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='ADIP-controller python cmd')
    parser.add_argument('-s', type=str, nargs='+', help='处理容器和helm包')
    args = parser.parse_args()

    repository = args.s[0]
    source_path = args.s[1]
    dest_path = args.s[2]

    file_nmaes = os.listdir(source_path)
    for file_name in file_nmaes:
        if file_name.endswith(".tar"):
            if file_name.startswith("hados_dev-"):
                settle = '-' + file_name.split("hados_dev-", 1)[1].split('.')[0].split('-')[-1]
                itemname = file_name.split("hados_dev-", 1)[1].split('.', 1)[0].split(settle)[0]
            else:
                settle = '-' + file_name.split("hados-", 1)[1].split('.')[0].split('-')[-1]
                itemname = file_name.split("hados-", 1)[1].split('.', 1)[0].split(settle)[0]
            
            container_path = "{}{}".format(dest_path, itemname)
            if os.path.exists(container_path):
                print(container_path, "is exist")
            else:
                os.mkdir(container_path)

            if "telemetry" in file_name:
                os.system("python3 docker_pull.py harbor.yusur.tech/telemetry/node-exporter:1.5.0 && \
                          mv telemetry_node-exporter.tar {}/{}\
                          ".format(container_path, "hados-node_exporter-1.5.0.x86_64.tar"))
                os.system("python3 docker_pull.py harbor.yusur.tech/telemetry/cadvisor:latest && \
                          mv telemetry_cadvisor.tar {}/{}\
                          ".format(container_path, "hados-cadvisor-latest.x86_64.tar"))
                os.system("python3 docker_pull.py harbor.yusur.tech/telemetry/opentelemetry-collector-contrib:0.77.0 && \
                          mv telemetry_opentelemetry-collector-contrib.tar {}/{}\
                          ".format(container_path, "hados-opentelemetry-collector-contrib-0.77.0.x86_64.tar"))

            print("cp -u {}{} {}".format(source_path, file_name, container_path))
            os.system("cp -u {}{} {}".format(source_path, file_name, container_path))
        elif file_name.endswith(".zip"):
            if file_name.startswith("hados_dev-"):
                settle = '-conf-' + file_name.split("hados_dev-", 1)[1].split('.')[0].split('-')[-1]
                itemname = file_name.split("hados_dev-", 1)[1].split('.', 1)[0].split(settle)[0]
            else:
                settle = '-conf-' + file_name.split("hados-", 1)[1].split('.')[0].split('-')[-1]
                itemname = file_name.split("hados-", 1)[1].split('.', 1)[0].split(settle)[0]
            
            container_path = "{}{}".format(dest_path, itemname)
            if os.path.exists(container_path):
                print(container_path, "is exist")
            else:
                os.mkdir(container_path)
            print("cp -u {}{} {}".format(source_path, file_name, container_path))
            os.system("cp -u {}{} {}".format(source_path, file_name, container_path))

    container_nmaes = os.listdir(dest_path)
    for container_name in container_nmaes:
        os.system("cd {}{} && unzip *.zip && rm -rf *.zip".format(dest_path, container_name))
        #os.system("mv {}{}{} {}".format(dest_path, container_name, "/config/*.yaml", "../"))

