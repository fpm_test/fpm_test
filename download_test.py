#!/usr/bin/env python
# coding=utf-8
'''
Author       : yangchujie
Date         : 2023-08-09 11:43:38
LastEditors  : yangchujie
LastEditTime : 2023-08-15 09:21:47
FilePath     : download_test.py
Description  : unittest for functions in download.py
Copyright (c) 2023 Yusur Tech. All Rights Reserved.
'''

import unittest
import download

class TestDownloadHADOSPackages(unittest.TestCase):

    def test_rpm_packages(self):
        download.downloadPackagesWithProjectAndRepository("HADOS_ovs", "CentOS-Stream-8")

    def test_deb_packages(self):
        download.downloadPackagesWithProjectAndRepository("HADOS_rdma", "Ubuntu_20.04")

    def test_container_packages(self):
        download.downloadPackagesWithProjectAndRepository("HADOS_opi-yusur-bridge", "containers")

    def test_chart_packages(self):
        download.downloadPackagesWithProjectAndRepository("HADOS_SeltTest_yangcj", "charts")

    def test_hados_rpm_packages(self):
        download.downloadPackagesWithProjectAndRepository("HADOS", "openEuler-22.03-LTS")

    def test_hados_container_packages(self):
        download.downloadPackagesWithProjectAndRepository("HADOS", "containers")

    def test_hados_chart_packages(self):
        download.downloadPackagesWithProjectAndRepository("HADOS", "charts")

if __name__ == '__main__':
    unittest.main()
