#!/usr/bin/env python
# coding=utf-8
'''
Author       : SunXuDong
Date         : 2023-02-20 23:24:48
LastEditors: liangsw@yusur.tech
LastEditTime: 2023-11-22 11:02:40
FilePath     : download.py
Description  : 
Copyright (c) 2023 Yusur Tech. All Rights Reserved.
'''

import argparse
import concurrent.futures
import json
import pathlib
import requests
import shutil
import time
import os
import urllib.request
import sys
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

# This BBS_TOKEN is the 'adip' authentication token of ordinary users registered 
# on the BuildBoxService platform by the ADIP group, and it is dedicated for 
# downloading project's build package.
BBS_TOKEN="YWRpcDpBRElQfiZrSUsjcU42bEVFXmw="

def getRequestSession():
    s = requests.Session()
    retry = Retry(
            total=8,
            backoff_factor=3,
            status_forcelist=[400, 429, 500, 502, 503, 504],
        )
    adapter = HTTPAdapter(max_retries=retry)
    s.mount('http://', adapter)
    s.mount('https://', adapter)
    return s

def downloadPackage(packageURL, savePath):
    try:
        s = getRequestSession()
        response = s.get(packageURL, headers={
            "Authorization": "Basic {bbs_token}".format(bbs_token=BBS_TOKEN)
        })
        if response.status_code == 200:
            with open(savePath, 'wb') as f:
                for chunk in response.iter_content(chunk_size=1024):
                    if chunk:
                        f.write(chunk)
        else:
            raise Exception("ADIP-Controller API get download package info failed: ", packageURL)
    except Exception as e:
        raise e
    finally:
        s.close()
    return (savePath,)

def downloadPackagesWithProjectAndRepository(version, repository):

    # Get version 
    if not version:
        print("version is not provided in JSON data.")
        return

    # Get repository 
    if not repository:
        print("repository is not provided in JSON data.")
        return

    url = "http://adip.yusur.tech/apiv1/download"
    payload = json.dumps({
        "version": version
    })
    headers = {
        'Content-Type': 'application/json',
        "Authorization": "Basic {bbs_token}".format(bbs_token=BBS_TOKEN)
    }

    # API request
    try:
        s = getRequestSession()
        response = s.request("GET", url, headers=headers, data=payload)

        if response.status_code != 200:
            sys.exit("ADIP-Controller API request failed.")
        
        # download packages: rpm and deb
        parseResponseToStartDownload(version, repository, response)
        
        # download packages: containers and charts
        parseResponseToStartDownload(version, "containers", response)
        parseResponseToStartDownload(version, "charts", response)
    except Exception as e:
        sys.exit(e)
    finally:
        s.close()

def parseResponseToStartDownload(version, repository, response):
    data = json.loads(response.text)

    if "data" not in data or data["data"] is None:
        sys.exit("ADIP-Controller API get download packages info failed: data attribute is None.")
    
    if "files" not in data["data"] or data["data"]["files"] is None:
        sys.exit("ADIP-Controller API get download packages info failed: files attribute is None.")

    # JSON parsing version and files
    files = data["data"]["files"]

    # render download URL
    url_prefix = "https://build.yusur.tech/build/" + version + os.path.sep
    # mirrors URL for rpm and deb package
    mirrors_url_prefix = "https://mirrors.yusur.tech/hados/" + version + os.path.sep

    # Default concurrent 1-thread download to avoid bbs 503 exception
    with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
        futures = []
        for repo_type, package_data in files.items():
            # only download user input args repository package
            if repository != repo_type:
                continue

            # Create a directory if it does not exist
            os_dir = os.path.join(version, repo_type)
            shutil.rmtree(os_dir, ignore_errors=True)
            os.makedirs(os_dir, exist_ok=True)

            for arch, package_list in package_data.items():
                for package, files_list in package_list.items():
                    # Download the package file
                    for filename in files_list:
                        package_file = os.path.join(os_dir, filename)
                        if repo_type == "containers":
                            package_url = url_prefix + repo_type + os.path.sep + arch + os.path.sep + package + os.path.sep + filename
                            future = executor.submit(downloadPackage, package_url, package_file)
                            futures.append(future)
                            print(f"{repo_type} {package} {filename} downloading...")
                            #add zip package for download
                            settle_new = "-conf-" + filename.split('.')[0].split('-')[-1]
                            settle_old = "-" + filename.split('.')[0].split('-')[-1]
                            filename = filename.replace(settle_old, settle_new).replace("tar", "zip")
                            package_file = os.path.join(os_dir, filename)
                            package_url = mirrors_url_prefix + repo_type + os.path.sep + filename
                        elif repo_type == "charts":
                            package_url = url_prefix + repo_type + os.path.sep + arch + os.path.sep + package + os.path.sep + filename
                        else:
                            if filename.endswith(".deb"):
                                arch = "amd64"
                            package_url = mirrors_url_prefix + repo_type + os.path.sep + arch + os.path.sep + filename
                        future = executor.submit(downloadPackage, package_url, package_file)
                        futures.append(future)
                        print(f"{repo_type} {package} {filename} downloading...") 

        # Wait for all downloads to complete
        for future in concurrent.futures.as_completed(futures):
            #Judge whether the download is successful
            if future.exception():
                print(f"Download failed: {future.exception()}")
            else:
                print(f"{future.result()[0]} downloaded.")
        
        print("All downloads are completed.")     

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='ADIP-controller python cmd')
    parser.add_argument('-q', type=str, nargs='+', help='Only download packages file')
    args = parser.parse_args()

    if args.q is not None:
        downloadPackagesWithProjectAndRepository(args.q[0], args.q[1])
    else:
        print('Please use following parameters: -q')
