#!/usr/bin/env python
# coding=utf-8
'''
Author       : liangsw@yusur.tech
Date         : 2023-08-04 10:33:18
FilePath     : copy.py
Description  : 
Copyright (c) 2023 by liangsw email: liangsw@yusur.tech, All Rights Reserved.
'''

import os
import os.path
import yaml
import argparse

#根据yaml文件copy文件以及生成repodate
def copy_file(args):

    repository = args[0]
    source_path = args[1]
    dest_path = args[2]
    target_name = args[4]
    dest_path_container = "{}{}".format(dest_path, "../hados/")
    dest_path_helm = "{}{}".format(dest_path, "../helm/")
    source_path_container = "{}{}".format(source_path, "../containers")
    source_path_charts = "{}{}".format(source_path, "../charts")

    #读取yaml文件
    pwd_path = os.path.abspath(".")
    file_path = os.path.join(pwd_path, "products.yaml")

    #解析yaml文件
    with open(file_path, mode="r", encoding="utf-8") as f:
        if repository == "CentOS-Stream-8":
            yamlConf = yaml.load(f.read())
        else:
            yamlConf = yaml.load(f.read(), Loader=yaml.FullLoader)

    target_list_name = yamlConf["product"]

    #根据yaml文件处理文件
    for product_name in target_list_name.keys():
        if product_name == args[3]:
            for target_names, rpm_names in target_list_name[product_name].items():
                if target_name == target_names:
                    for rpm_name in rpm_names:
                        if repository == "Ubuntu_20.04":
                            rpm_name = rpm_name[:5] + '-' + rpm_name[6:]
                        #复制rpm包和deb包
                        file_nmaes = os.listdir(source_path)
                        for file_name in file_nmaes:
                            if file_name.startswith(rpm_name):
                                #if file_name.endswith(".rpm") or file_name.endswith(".deb"):
                                    print("cp -u {}{} {}".format(source_path, file_name, dest_path))
                                    os.system("cp -u {}{} {}".format(source_path, file_name, dest_path))
                        #复制容器tar包和容器配置文件zip压缩包
                        file_nmaes = os.listdir(source_path_container)
                        for file_name in file_nmaes:
                            rpm_name = "hados_dev-{}".format(rpm_name)
                            if file_name.startswith(rpm_name):
                                #if file_name.endswith(".zip") or file_name.endswith(".tar"):
                                    container_path = "{}{}".format(dest_path_container, rpm_name)
                                    if os.path.exists(container_path):
                                        print("exist")
                                    else:
                                        os.mkdir(container_path)
                                    print("cp -u {}{} {}".format(source_path_container, file_name, container_path))
                                    os.system("cp -u {}{} {}".format(source_path_container, file_name, container_path))
                        #复制chart的tgz包
                        #由于目前没有chart包，传入chart包地址会报no such file ，暂时使用rpm包路径避免报错
                        #file_nmaes = os.listdir(source_path_charts)
                        file_nmaes = os.listdir(source_path)
                        for file_name in file_nmaes:
                            if file_name.startswith(rpm_name):
                                #if file_name.endswith(".tgz"):
                                    print("cp -u {}{} {}".format(source_path_charts, file_name, dest_path_helm))
                                    #os.system("cp -u {}{} {}".format(source_path_charts, file_name, dest_path_helm))
                    #解压缩container的配置文件zip包
                    container_nmaes = os.listdir(dest_path_container)
                    for container_name in container_nmaes:
                        print(container_name)
                        os.system("cd {}{} && unzip *.zip && rm -rf *.zip".format(dest_path_container, container_name))
                        os.system("mv {}{}{} {}".format(dest_path_container, container_name, "/config/*.yaml", "../"))
                    #生成ubuntu的package包或其他linux系统的repodate
                    if repository == "Ubuntu_20.04":
                        os.system("cd {}/.. && dpkg-scanpackages repo /dev/null | gzip > {}Packages.gz".format(dest_path, dest_path))
                    else:
                        os.system("createrepo {}".format(dest_path))

#copy全部文件以及生成repodate
def copy_all_file(args):
    
    repository = args[0]
    source_path = args[1]
    dest_path = args[2]
    dest_path_container = "{}{}".format(dest_path, "../hados/")
    dest_path_helm = "{}{}".format(dest_path, "../helm/")
    source_path_container = "{}{}".format(source_path, "../containers")
    source_path_charts = "{}{}".format(source_path, "../charts")

    #复制rpm包和deb包
    file_nmaes = os.listdir(source_path)
    for file_name in file_nmaes:
        #if file_name.endswith(".rpm") or file_name.endswith(".deb"):
            print("cp -u {}{} {}".format(source_path, file_name, dest_path))
            os.system("cp -u {}{} {}".format(source_path, file_name, dest_path))
    #复制容器tar包和容器配置文件zip压缩包
    file_nmaes = os.listdir(source_path_container)
    for file_name in file_nmaes:
        #if file_name.endswith(".zip") or file_name.endswith(".tar"):
            container_name = file_name.split('-')[1].split('-')[0]
            container_path = "{}{}".format(dest_path_container, container_name)
            if os.path.exists(container_path):
                print("exist")
            else:
                os.mkdir(container_path)
                print("cp -u {}{} {}".format(source_path_container, file_name, container_path))
                os.system("cp -u {}{} {}".format(source_path_container, file_name, container_path))
    #复制chart的tgz包
    #由于目前没有chart包，传入chart包地址会报no such file ，暂时使用rpm包路径避免报错
    #file_nmaes = os.listdir(source_path_charts)
    file_nmaes = os.listdir(source_path)
    for file_name in file_nmaes:
        #if file_name.endswith(".tgz"):
            print("cp -u {}{} {}".format(source_path_charts, file_name, dest_path_helm))
            #os.system("cp -u {}{} {}".format(source_path_charts, file_name, dest_path_helm))

    #解压缩container的配置文件zip包
    container_nmaes = os.listdir(dest_path_container)
    for container_name in container_nmaes:
        print(container_name)
        os.system("cd {}{} && unzip *.zip && rm -rf *.zip".format(dest_path_container, container_name))
        os.system("mv {}{}{} {}".format(dest_path_container, container_name, "/config/*.yaml", "../"))

    #生成ubuntu的package包或其他linux系统的repodate
    if repository == "Ubuntu_20.04":
        os.system("cd {}/.. && dpkg-scanpackages repo /dev/null | gzip > {}Packages.gz".format(dest_path, dest_path))
    else:
        os.system("createrepo {}".format(dest_path))

#根据linux版本生成.repo或.list文件
def autogeneration(args):

    dest_path = args[1]
    repository = args[0]
'''
    #读取yaml文件
    pwd_path = os.path.abspath(".")
    file_path = os.path.join(pwd_path, "products.yaml")

    #解析yaml文件
    with open(file_path, mode="r", encoding="utf-8") as f:
        if repository == "CentOS-Stream-8":
            yamlConf = yaml.load(f.read())
        else:
            yamlConf = yaml.load(f.read(), Loader=yaml.FullLoader)

    target_list_name = yamlConf["product"]

    #根据yaml文件处理文件
    for product_name in target_list_name.keys():
        if target_list_name[product_name]:
            if repository == "openEuler-22.03-LTS" or args.c[0] == "CentOS-Stream-8":
    for target_names, rpm_names in target_dict:
        if target_name == target_names:
            os.system("echo {} > {}{}-{}.repo".format("[hados]", dest_path, product_name, target_name))
            os.system("echo {} >> {}{}-{}.repo".format("name=HADOS", dest_path, product_name, target_name))
            os.system("echo {}{}-{} >> {}{}-{}.repo".format("baseurl=file:///opt/yusur/repo/",product_name, target_name, dest_path, product_name, target_name))
            os.system("echo {} >> {}{}-{}.repo".format("enabled=1", dest_path, product_name, target_name))
            os.system("echo {} >> {}{}-{}.repo".format("gpgcheck=0", dest_path, product_name, target_name))

    #ubuntu生成.list文件
    def autogeneration(target_dict, dest_path, target_name, product_name):
    for target_names, rpm_names in target_dict:
        if target_name == target_names:        
            os.system("echo {} > {}{}-{}.list".format("#", dest_path, product_name, target_name))
            os.system("echo {} >> {}{}-{}.list".format("# YUSUR HADOS public repository configuration file.", dest_path, product_name, target_name))
            os.system("echo {} >> {}{}-{}.list".format("# For more information, refer to http://www.yusur.tech", dest_path, product_name, target_name))
            os.system("echo {} >> {}{}-{}.list".format("#", dest_path, product_name, target_name))
            os.system("echo {} >> {}{}-{}.list".format(" ", dest_path, product_name, target_name))
            os.system("echo {} {}-{}{} >> {}{}-{}.list".format("deb [trusted=yes] file:///opt/yusur/repo/",product_name, target_name, "/", dest_path, product_name, target_name))
'''
            
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='ADIP-controller python cmd')
    parser.add_argument('-a', type=str, nargs='+', help='根据yaml拷贝文件的路径参数')
    parser.add_argument('-c', type=str, nargs='+', help='生成源文件')
    parser.add_argument('-s', type=str, nargs='+', help='拷贝全部文件的路径参数')
    args = parser.parse_args()

    if args.a is not None:
        copy_file(args.a)
    elif args.c is not None:
        autogeneration(args.c)
    elif args.s is not None:
        copy_all_file(args.s)
