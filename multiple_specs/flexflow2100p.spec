Name:		flexflow2100p
Version:	1.0
Release:	1
Summary:	HADOS 

Group:		HADOS
License:	GPL

Source0: HADOS_target.tar.gz

%define HADOS_VER HADOS

%if "%{_repository}" == "Ubuntu_20.04"
BuildRequires:python3,python3-requests,python3-yaml
%else
BuildRequires:python3,python3-requests,python3-pyyaml
%endif

%if 0%{?rhel} >= 8 || "%{_vendor}" == "openEuler"
BuildRequires: createrepo
%else
BuildRequires: 
%endif

%description
HADOS flexflow2100p

%prep
%setup -q -n HADOS_target

%build
cd %{_builddir}/HADOS_target
%{__mkdir_p} %{HADOS_VER}

python3 download.py -q %{HADOS_VER} %_repository

%install
%{__install} -m 0755 -d %{buildroot}/opt/yusur
%{__install} -m 0755 -d %{buildroot}/opt/yusur/repo
%{__install} -m 0755 -d %{buildroot}/opt/yusur/hados
%{__install} -m 0755 -d %{buildroot}/opt/yusur/helm

%if 0%{?rhel} >= 8 || "%{_vendor}" == "openEuler"
%{__install} -m 0755 -d %{buildroot}%{_sysconfdir}/yum.repos.d
%{__install} -p -m 0644 -t %{buildroot}%{_sysconfdir}/yum.repos.d/ hados.repo
%else
%{__install} -m 0755 -d %{buildroot}%{_sysconfdir}/apt/sources.list.d
%{__install} -p -m 0644 -t %{buildroot}%{_sysconfdir}/apt/sources.list.d/ hados.list
%endif

%if "%{_repository}" == "openEuler-22.03-LTS_dpu"
python3 copy_package.py -a %{_repository} %{HADOS_VER}/%{_repository}/ %{buildroot}/opt/yusur/repo/ "flexflow2100p" "dpu"
%else
python3 copy_package.py -a %{_repository} %{HADOS_VER}/%{_repository}/ %{buildroot}/opt/yusur/repo/ "flexflow2100p" "host"
%endif

%if "%{_repository}" == "Ubuntu_20.04"

%post
apt-get update

%endif

%files
/opt/yusur/repo/*
%if 0%{?rhel} >= 8 || "%{_vendor}" == "openEuler"
%{_sysconfdir}/yum.repos.d/hados.repo
%else
%{_sysconfdir}/apt/sources.list.d/hados.list
%endif

%changelog
* Wed Aug 16 2023 LiangShiWei <liangsw@yusur.tech> 1.0
- Add a parsing script for the yaml file
